# UnitTestAgenda5
***Generación de pruebas Unitarias***

Se han creado en la clase `Calculator` métodos para restar, multiplicar, dividir y elevar un número a una potencia especificada. Se han agregado las pruebas correspondientes para cada uno de estos métodos en la clase `CalculatorTest`

**Integrantes**
 - David Bermúdez
 - Leonardo Flores
 - Leandro Fonseca
 - Jasser Gutiérrez