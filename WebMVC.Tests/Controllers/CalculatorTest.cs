﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }



 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Substract()
        {
            //Arrange (Organizar)
            int value1 = 3;
            int value2 = 2;
            int expected = 1;

            //Act(Actuar)
            int actual = SystemUnderTest.Substract(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Multiply()
        {
            //Arrange (Organizar)
            int value1 = 3;
            int value2 = 3;
            int expected = 9;

            //Act(Actuar)
            int actual = SystemUnderTest.Multiply(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Divide()
        {
            //Arrange (Organizar)
            decimal value1 = 10;
            decimal value2 = 2;
            decimal expected = 5;

            //Act(Actuar)
            decimal actual = SystemUnderTest.Divide(value1, value2);

            //Assert (Afirmar)
            Assert.AreEqual(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Pow()
        {
            //Arrange (Organizar)
            double numberToBeRaised = 3;
            double power = 3;
            double expected = 27;

            //Act(Actuar)
            double actual = SystemUnderTest.RaiseToPower(numberToBeRaised, power);

            //Assert (Afirmar)
            Assert.AreEqual(expected, actual, "Error, valores no coinciden");
        }
        
        [TestMethod]
        public void SquareRoot() 
        {
            //Arrange (Organizar)
            double numberToSquareRoot = 81;
            double expected = 9;

            //Act (Actuar)
            double actual = SystemUnderTest.SquareRoot(numberToSquareRoot);

            //Assert (Afirmar)
            Assert.AreEqual(expected, actual, "Error, valores no coinciden");
        }

    }
}
