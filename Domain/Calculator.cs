﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2;
        }

        public int Substract(int value1, int value2)
        {
            return value1 - value2;
        }

        public int Multiply(int value1, int value2)
        {
            return value1 * value2;
        }

        public decimal Divide(decimal value1, decimal value2)
        {
            return value1 / value2;
        }

        public double RaiseToPower(double value1, double value2)
        {
            return Math.Pow(value1, value2);
        }
        
        public double SquareRoot(double value1)
        {
            return Math.Sqrt(value1);
        }

    }
}
